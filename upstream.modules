<?xml version="1.0"?><!--*- mode: nxml; indent-tabs-mode: nil -*-->
<!DOCTYPE moduleset SYSTEM "moduleset.dtd">
<?xml-stylesheet type="text/xsl" href="moduleset.xsl"?>
<!-- vim:set ts=2 expandtab: -->
<moduleset>

  <!-- Official repositories, not used by default, using provided tars instead. -->

  <repository type="tarball" name="simgrid" href="https://framagit.org/simgrid/simgrid/uploads/b086147968e111e7b7d7e27e8c4b11c5/"/>
  <repository type="git" name="simgrid-dev" href="https://github.com/"/>
  <repository type="tarball" name="openbabel" href="https://github.com/openbabel/openbabel/releases/download/openbabel-3-1-1/"/>
  <repository type="tarball" name="ase" href="https://pypi.org/packages/source/a/ase/"/>
  <repository type="tarball" name="dill" href="https://github.com/uqfoundation/dill/releases/download/dill-0.3.3/"/>
  <repository type="git" name="sphinx" href="https://github.com/rowanG077/"/>

  <repository type="git" name="L_Sim" href="https://gitlab.com/l_sim/"/>
  <repository type="tarball" name="local" href="./"/>

  <include href="gnome.modules"/>
  <include href="lowlevel.modules"/>
  <include href="bio.modules"/>
  <include href="optional.modules"/>
  <include href="sirius.modules"/>
  <include href="hpc-upstream.modules"/>

  <autotools id="GaIn">
    <branch repo="local" module="GaIn-1.0.tar.gz"
            version="1.0"/>
  </autotools>

  <autotools id="v_sim-dev" autogen-sh="autogen.sh+configure" autogenargs="--with-abinit --with-archives --with-openbabel --with-cube --without-strict-cflags">
    <!-- <branch repo="local-optional" module="v_sim-dev.tar.bz2" -->
    <branch repo="L_Sim" module="v_sim"
            version="3.99" checkoutdir="v_sim-dev">
      <patch file="v_sim-dev.patch" />
    </branch>
    <dependencies>
      <dep package="PyYAML"/>
      <dep package="openbabel"/>
      <dep package="intltool"/>
      <dep package="libglu1-mesa-dev"/>
      <if condition-set="python">
        <dep package="pygobject"/>
      </if>
    </dependencies>
  </autotools>

  <distutils id="sphinx-fortran">
    <branch repo="L_Sim" module="sphinx-fortran" revision="lsim" />
    <dependencies>
      <dep package="lsim-f2py"/>
    </dependencies>
  </distutils>

  <distutils id="lsim-f2py">
    <branch repo="L_Sim" module="f2py" />
  </distutils>

  <distutils id="sphinx-multibuild">
    <branch repo="sphinx" module="sphinx-multibuild" revision="1.2.0" />
  </distutils>

  <cmake id="openbabel-old" use-ninja="no">
    <branch repo="local" module="openbabel-2.4.1.tar.gz" version="2.4.1" checkoutdir="openbabel"/>
  </cmake>

  <cmake id="openbabel" use-ninja="no">
    <branch repo="openbabel" module="openbabel-3.1.1-source.tar.bz2" version="3.1.1" checkoutdir="openbabel"/>
    <cmakeargs value="-DPYTHON_BINDINGS=Yes -DRUN_SWIG=ON"/>
    <dependencies>
      <dep package="eigen3"/>
      <if condition-set="oldmachine">
        <dep package="swig"/>
        <dep package="cmake-latest"/>
      </if>
    </dependencies>
  </cmake>

  <cmake id="simgrid" use-ninja="no">
    <branch repo="simgrid" module="SimGrid-3.22.tar.gz"
            version="3.22"/>
  </cmake>

  <cmake id="simgrid-dev" use-ninja="no">
    <branch repo="simgrid-dev"  module="simgrid/simgrid"
            checkoutdir="simgrid-dev"/>
    <dependencies>
      <dep package="libboost-dev"/>
    </dependencies>
  </cmake>

  <distutils id="ase">
    <branch repo="ase" module="ase-3.20.1.tar.gz" version="3.20.1" checkoutdir="ase">
    </branch>
  </distutils>

  <distutils id="dill">
    <branch repo="dill" module="dill-0.3.3.tar.gz" version="0.3.3" checkoutdir="dill">
    </branch>
  </distutils>

  <metamodule id="client-bio-plugins">
    <dependencies>
      <dep package="biopython"/>
      <dep package="pdbfixer"/>
      <dep package="rdkit"/>
      <dep package="dnaviewer"/>
      <dep package="openbabel"/>
      <dep package="openmmforcefields"/>
    </dependencies>
  </metamodule>

  <metamodule id="client-plugins">
    <dependencies>
      <if condition-set="devdoc">
        <dep package="sphinx-multibuild"/>
      </if>
      <if condition-set="ase">
        <dep package="ase"/>
      </if>
      <if condition-set="dill">
        <dep package="dill"/>
      </if>
      <if condition-set="bio">
        <dep package="client-bio-plugins"/>
      </if>
      <if condition-set="spg">
        <dep package="spglib"/>
      </if>
      <if condition-set="vsim">
        <dep package="v_sim-dev"/>
      </if>
    </dependencies>
  </metamodule>

  <metamodule id="client-upstream-suite">
    <dependencies>
      <dep package="PyYAML"/>
      <dep package="client-plugins"/>
    </dependencies>
  </metamodule>

  <metamodule id="core-upstream-suite">
    <dependencies>
      <if condition-unset="sirius">
        <dep package="libxc"/>
      </if>
      <dep package="GaIn"/>
      <dep package="ntpoly"/>
      <dep package="bigdft-plugins"/>
    </dependencies>
  </metamodule>

  <metamodule id="bigdft-plugins">
    <dependencies>
      <dep package="futile-plugins"/>
      <if condition-set="sirius">
        <dep package="sirius"/>
      </if>
    </dependencies>
  </metamodule>

  <metamodule id="futile-plugins">
    <dependencies>
      <if condition-set="simulation">
        <dep package="simgrid-dev"/>
      </if>
      <if condition-set="devdoc">
        <dep package="sphinx-fortran"/>
      </if>
      <if condition-set="python">
        <dep package="pygobject"/>
      </if>
    </dependencies>
  </metamodule>

  <metamodule id="upstream-suite">
    <dependencies>
      <dep package="client-upstream-suite"/>
      <dep package="core-upstream-suite"/>
    </dependencies>
  </metamodule>

</moduleset>
